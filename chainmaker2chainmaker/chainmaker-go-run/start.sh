#!/bin/bash

cd chainmaker/bin
./start.sh
cd ../../

sleep 10

#./cmc client contract user create \
#--contract-name=crosschain1 \
#--runtime-type=DOCKER_GO \
#--byte-code-path=./contract/crosschain1.7z \
#--version=1.0 \
#--sdk-conf-path=./sdk_config_chain2.yml \
#--admin-key-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.key \
#--admin-crt-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.crt \
#--sync-result=true \
#--params="{}"
#
#./cmc client contract user create \
#--contract-name=crosschain2 \
#--runtime-type=DOCKER_GO \
#--byte-code-path=./contract/crosschain2.7z \
#--version=1.0 \
#--sdk-conf-path=./sdk_config_chain3.yml \
#--admin-key-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.key \
#--admin-crt-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.crt \
#--sync-result=true \
#--params="{}"
#
#./cmc client contract user create \
#--contract-name=crosschain3 \
#--runtime-type=DOCKER_GO \
#--byte-code-path=./contract/crosschain3.7z \
#--version=1.0 \
#--sdk-conf-path=./sdk_config_chain4.yml \
#--admin-key-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.key \
#--admin-crt-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.crt \
#--sync-result=true \
#--params="{}"

./cmc client contract user create \
--contract-name=erc20_01 \
--runtime-type=DOCKER_GO \
--byte-code-path=./contract/erc20_01.7z \
--version=1.0 \
--sdk-conf-path=./sdk_config_chain2.yml \
--admin-key-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.key \
--admin-crt-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.crt \
--sync-result=true \
--params="{\"token_bytes\":\"{\\\"name\\\":\\\"test1\\\",\\\"symbol\\\":\\\"test1\\\",\\\"decimals\\\":10,\\\"TotalSupply\\\":10000}\"}"

./cmc client contract user invoke \
--contract-name=erc20_01 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain2.yml \
--params="{\"method\":\"minter\",\"to\":\"69cb09210603496890a5d9423f1041ef7c985aa962c0f76e8f985d2efbacb1bb\",\"value\":\"100000\"}" \
--sync-result=true

./cmc client contract user create \
--contract-name=erc20_02 \
--runtime-type=DOCKER_GO \
--byte-code-path=./contract/erc20_02.7z \
--version=1.0 \
--sdk-conf-path=./sdk_config_chain3.yml \
--admin-key-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.key \
--admin-crt-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.crt \
--sync-result=true \
--params="{\"token_bytes\":\"{\\\"name\\\":\\\"test2\\\",\\\"symbol\\\":\\\"test2\\\",\\\"decimals\\\":10,\\\"TotalSupply\\\":10000}\"}"

./cmc client contract user invoke \
--contract-name=erc20_02 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain3.yml \
--params="{\"method\":\"minter\",\"to\":\"69cb09210603496890a5d9423f1041ef7c985aa962c0f76e8f985d2efbacb1bb\",\"value\":\"100000\"}" \
--sync-result=true

./cmc client contract user create \
--contract-name=erc20_03 \
--runtime-type=DOCKER_GO \
--byte-code-path=./contract/erc20_03.7z \
--version=1.0 \
--sdk-conf-path=./sdk_config_chain4.yml \
--admin-key-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.key \
--admin-crt-file-paths=./chainmaker/config/wx-org.chainmaker.org/certs/user/admin1/admin1.tls.crt \
--sync-result=true \
--params="{\"token_bytes\":\"{\\\"name\\\":\\\"test3\\\",\\\"symbol\\\":\\\"test3\\\",\\\"decimals\\\":10,\\\"TotalSupply\\\":10000}\"}"

./cmc client contract user invoke \
--contract-name=erc20_03 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain4.yml \
--params="{\"method\":\"minter\",\"to\":\"69cb09210603496890a5d9423f1041ef7c985aa962c0f76e8f985d2efbacb1bb\",\"value\":\"100000\"}" \
--sync-result=true

echo "chainmaker-go start"