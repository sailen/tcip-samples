#!/bin/bash

pid=$(cat ./pid | sed -n "1p")

if [ $1 == up ]
then
	nohup ./tcip-chainmaker start -c ./config/tcip_chainmaker.yml > panic.log 2>&1 & echo $! > pid
	echo "tcip-chainmaker-2 start"
	cat ./pid
	exit
fi
if [ $1 == down ]
then
	kill $pid
	echo "tcip-chainmaker-2 stop: $pid"
	exit
fi
echo "error parameter"