#!/bin/bash

./cmc client contract user invoke \
--contract-name=erc20_01 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain2_client1.yml \
--params="{\"method\":\"balanceOf\",\"name\":\"69cb09210603496890a5d9423f1041ef7c985aa962c0f76e8f985d2efbacb1bb\"}" \
--sync-result=true

./cmc client contract user invoke \
--contract-name=erc20_02 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain3_client1.yml \
--params="{\"method\":\"balanceOf\",\"name\":\"69cb09210603496890a5d9423f1041ef7c985aa962c0f76e8f985d2efbacb1bb\"}" \
--sync-result=true
