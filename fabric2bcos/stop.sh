#!/bin/bash

cd chainmaker-go-run/chainmaker/bin/
./stop.sh

cd ../../../fabric-samples/test-network
./network.sh down

docker rm VM-GO-wx-org.chainmaker.org

cd ../../bcos-run/
./stop_all.sh

cd ../tcip-fabric-1-run
./switch.sh down
cd ../tcip-bcos-2-run
./switch.sh down
cd ../tcip-relayer-1-run
./switch.sh down