#!/bin/bash

cd chainmaker-go-run/chainmaker/bin/
./stop.sh
docker rm VM-GO-wx-org.chainmaker.org

cd ../../../tcip-chainmaker-1-run
./switch.sh down
cd ../tcip-chainmaker-2-run
./switch.sh down
cd ../tcip-chainmaker-3-run
./switch.sh down
cd ../tcip-relayer-1-run
./switch.sh down
cd ../tcip-relayer-2-run
./switch.sh down