#!/bin/bash

nohup ./tcip-bcos start -c ./config/tcip_bcos.yml > panic.log 2>&1 & echo $! > pid

./tcip-bcos register -c config/tcip_bcos.yml

sleep 10

CHAINCONFIG=$(cat chain_config.json)
curl -k -H "Content-Type: application/json" -X POST -d "$CHAINCONFIG" https://localhost:19997/v1/ChainIdentity

REGISTER=$(cat register.json)
curl -k -H "Content-Type: application/json" -X POST -d "$REGISTER" https://localhost:19999/v1/GatewayRegister

ADDRESS1=$(cat ../bcos-run/address | sed -n "1p")

sleep 5

sed -i "s/ADDRESS1/$ADDRESS1/g" event_1_cross_1.json

EVENT=$(cat event_1_cross_1.json)
curl -k -H "Content-Type: application/json" -X POST -d "$EVENT" https://localhost:19997/v1/CrossChainEvent

echo "tcip-bcos-1 start"