#!/bin/bash

VERSION=$(cat ../version.txt)

if [ ! -e "./chainmaker-go-run/chainmaker/bin/chainmaker" ];then
    echo "请下载chainmaker的二进制文件到chainmaker-go-run/chainmaker/bin文件夹下，下载地址：https://git.chainmaker.org.cn/chainmaker/chainmaker-go/uploads/b7f6f31a96334c4ea49aab0d680bbf75/chainmaker-v2.3.2-linux-x86_64.tar.gz，并解压"
    exit 1
fi

if [ ! -e "./chainmaker-go-run/cmc" ];then
    echo "请下载cmc的二进制文件到chainmaker-go-run文件夹下，下载地址：https://git.chainmaker.org.cn/chainmaker/chainmaker-go/uploads/0d820ef4c4864db6b8b9b28540564fa2/cmc-v2.3.2-linux-x86_64.tar.gz，并解压"
    exit 1
fi

CODEURL="git.chainmaker.org.cn"
ISTAG="tags"

if [ $TCIP_TEST ];then
  CODEURL="git.code.tencent.com"
  ISTAG="origin"
fi

function startChainmaker() {
#  if [ ! -d "./chainmaker-go" ];then
#    git clone git@git.code.tencent.com:ChainMaker/chainmaker-go.git
#  else
#    echo "chainmaker-go文件夹已经存在"
#  fi
#
#  cd chainmaker-go
#  git checkout -b v2.3.1_qc origin/v2.3.1_qc
#  make
#  cp bin/chainmaker ../chainmaker-go-run/chainmaker/bin/
#  cd tools/cmc
#  go build
#  cp cmc ../../../chainmaker-go-run/
  cd chainmaker-go-run/
  ./start.sh
  cd ..
}

function startTcipRelayer() {
  if [ ! -d "./tcip-relayer" ];then
    git clone https://$CODEURL/chainmaker/tcip-relayer.git
  else
    echo "tcip-relayer文件夹已经存在"
  fi

  cd tcip-relayer
  git checkout -b $VERSION $ISTAG/$VERSION
  git pull
  make build
  cp tcip-relayer ../tcip-relayer-1-run/
  cp tcip-relayer ../tcip-relayer-2-run/
  cd ../tcip-relayer-1-run/
  ./start.sh
  cd ..
}

function startTcipChainmaker() {
  if [ ! -d "./tcip-chainmaker" ];then
    git clone https://$CODEURL/chainmaker/tcip-chainmaker.git
  else
    echo "tcip-chainmaker文件夹已经存在"
  fi

  cd tcip-chainmaker
  git checkout -b $VERSION $ISTAG/$VERSION
  git pull
  make build
  cp tcip-chainmaker ../tcip-chainmaker-1-run
  cp tcip-chainmaker ../tcip-chainmaker-2-run

  cd ../tcip-chainmaker-1-run/
  ./start.sh 1

  cd ../tcip-chainmaker-2-run/
  ./start.sh

  cd ..
}

startChainmaker
startTcipRelayer
startTcipChainmaker